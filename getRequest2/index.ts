import { AzureFunction, Context, HttpRequest } from "@azure/functions";
import { Db, MongoClient } from "mongodb";

import assert = require("assert");

const httpTrigger: AzureFunction = async function (
  context: Context,
  req: HttpRequest
): Promise<void> {
  context.log("HTTP trigger function processed a request.");
  const url =
    "   mongodb://containeris:hFof3N8b1SDxfcyQpOZcQCk8XSoXPDWL0I8KoKxWPZpY7Z25i2zkRdCIu1WCIwnCv8NN4Cyghd3kLbrLTebNhw==@containeris.mongo.cosmos.azure.com:10255/?ssl=true&replicaSet=globaldb&retrywrites=false&maxIdleTimeMS=120000&appName=@containeris@";
  var ObjectId = require('mongodb').ObjectId;
  const id = req.query.id;
  const status = req.query.status;
  const type = req.query.type;
  const identificatie = req.query.identificatie;
  const searchId = new ObjectId(id);
  const client = await MongoClient.connect(url)
  var reponseMessage;
  var responseStatus = 200;
  var responseArray = [];
  var query = new Object;

  if (id == null && status == null && type == null && identificatie == null) {
    responseStatus = 400;
    reponseMessage = "no query defined";
  }

  if (id != null){

    query["_id"] = searchId;

  }

  if (identificatie != null){

    query["identificatie"] = identificatie;

  }

  if (type != null){

    query["type"] = type;

  }

  if (status != null){

    query["status"] = status;

  }

  if (responseStatus != 400){
    reponseMessage = await client.db('containeris').collection('identification').find(query).sort({_id: -1}).forEach(function (doc) { responseArray.push(doc) });
    reponseMessage = { documents: responseArray };
  }

  await client.close;
  context.res = {
    status: responseStatus,
    body: reponseMessage
  };
};

export default httpTrigger;
